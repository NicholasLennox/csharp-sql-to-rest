﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SqlToRestDemo.Models;
using SqlToRestDemo.Repositories;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SqlToRestDemo.Controllers
{
    [Route("api/customers")]
    [ApiController]
    public class CustomerController : ControllerBase
    {
        private readonly CustomerRepository _customerRepository;

        public CustomerController(CustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        // GET: api/customers
        [HttpGet]
        public ActionResult<IEnumerable<Customer>> Get()
        {
            // Going to use our CustomerRepository to fetch all customers
            return Ok(_customerRepository.GetAllCustomers());
        }

        // GET api/customers/ALFKI
        [HttpGet("{id}")]
        public ActionResult<Customer> Get(string id)
        {
            // Going to use our CustomerRepository to fetch a specific customer
            return Ok(_customerRepository.GetCustomer(id));
        }

        // POST api/customers
        [HttpPost]
        public ActionResult Post(Customer customer)
        {
            // Going to use our CustomerRepository to add a new customer 
            bool success = _customerRepository.AddNewCustomer(customer);
            return CreatedAtAction(nameof(Get), new { id = customer.CustomerID}, success);
        }

        // PUT api/customers/ALFKI
        [HttpPut("{id}")]
        public ActionResult Put(string id, Customer customer)
        {
            // Check for bad request 
            if(id != customer.CustomerID)
            {
                return BadRequest();
            }
            // Going to use our CustomerRepository to update an existing customer 
            bool success = _customerRepository.UpdateCustomer(customer);
            return NoContent();
        }
    }
}
