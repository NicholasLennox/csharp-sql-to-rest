﻿using SqlToRestDemo.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace SqlToRestDemo.Repositories
{

    public class CustomerRepository
    {
        public List<Customer> GetAllCustomers()
        {
            List<Customer> custList = new List<Customer>();
            string sql = "SELECT CustomerID, CompanyName, ContactName, City FROM Customers";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer temp = new Customer();
                                temp.CustomerID = reader.GetString(0);
                                temp.CompanyName = reader.GetString(1);
                                temp.ContactName = reader.GetString(2);
                                temp.City = reader.GetString(3);
                                custList.Add(temp);
                            }
                        }
                    }
                }
            }catch (SqlException ex)
            {
                // Log to console
            }
            return custList;
        }

        public Customer GetCustomer(string id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerID, CompanyName, ContactName, City FROM Customers" +
                " WHERE CustomerID = @CustomerID";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", id);
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {                     
                            while (reader.Read())
                            {
                                customer.CustomerID = reader.GetString(0);
                                customer.CompanyName = reader.GetString(1);
                                customer.ContactName = reader.GetString(2);
                                customer.City = reader.GetString(3);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
            }
            return customer;
        }

        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customers(CustomerID,CompanyName,ContactName,City) " +
                "VALUES(@CustomerID,@CompanyName,@ContactName,@City)";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionHelper.GetConnectionstring()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@CustomerID", customer.CustomerID);
                        cmd.Parameters.AddWithValue("@CompanyName", customer.CompanyName);
                        cmd.Parameters.AddWithValue("@ContactName", customer.ContactName);
                        cmd.Parameters.AddWithValue("@City", customer.City);
                        success = cmd.ExecuteNonQuery() > 0 ? true:false;
                    }
                }
            }
            catch (SqlException ex)
            {
                // Log to console
            }
            return success;
        }

        public bool UpdateCustomer(Customer customer)
        {
            throw new NotImplementedException();
        }
    }
}
